const db = require('../db')
const Exercise = require('../model/exercise.model')

exports.findByUser = async (userId) => {
    let con = await db.connect()
    
    let types = await Exercise.find({user: userId})
    con.disconnect()
    
    return types
}

exports.save = async (data) => {
    let con = await db.connect()
    let exercise = new Exercise(data)
    
    await exercise.save()
    con.disconnect()
}