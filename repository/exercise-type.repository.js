const db = require('../db')
const ExerciseType = require('../model/exercise-type.model')

exports.findAll = async () => {
    let con = await db.connect()
    
    let types = await ExerciseType.find()
    con.disconnect()
    
    return types
}