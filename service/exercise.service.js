const repository = require('../repository/exercise.repository')

exports.findByUser = async (userId) => {
    return await repository.findByUser(userId)
}

exports.save = async (data) => {
    await repository.save(data)
}