const repository = require('../repository/exercise.repository')

exports.findAll = async () => {
    return repository.findAll()
}