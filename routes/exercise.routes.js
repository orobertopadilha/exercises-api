const service = require('../service/exercise.service')

module.exports = (app) => {

    app.get('/exercises', async (req, res) => {
        let exercises = await service.findByUser(req.userId)
        res.json(exercises)
    })

    app.post('/exercises', async (req, res) => {
        await service.save(req.body)
        res.end()
    })

}