const service = require('../service/exercise-type.service')

module.exports = (app) => {

    app.get('/exercise-types', async (req, res) => {
        let types = await service.findAll()
        res.json(types)
    })

}