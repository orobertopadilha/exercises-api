const express = require('express')
const cors = require('cors')

const exerciseRoutes = require('./routes/exercise.routes')
const exerciseTypeRoutes = require('./routes/exercise-type.routes')

const authInterceptor = require('token-api/interceptor/auth.interceptor')

const app = express()

app.use(cors())
app.use(express.json())

authInterceptor(app)

exerciseRoutes(app)
exerciseTypeRoutes(app)

app.listen(8082, () => {
    console.log('Server is up!')
})