const mongoose = require('mongoose')

const exerciseTypeSchema = mongoose.Schema({
    name: String
})

module.exports = mongoose.model('ExerciseType', exerciseTypeSchema)