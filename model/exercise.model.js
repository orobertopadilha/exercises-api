const mongoose = require('mongoose')

const exerciseSchema = mongoose.Schema({
    duration: Number,
    date: Date,
    type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ExerciseType'
    },
    user: String
})

module.exports = mongoose.model('Exercise', exerciseSchema)